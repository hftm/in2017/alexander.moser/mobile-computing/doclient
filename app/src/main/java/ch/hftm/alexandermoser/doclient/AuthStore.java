package ch.hftm.alexandermoser.doclient;

import android.content.Context;
import android.content.SharedPreferences;

public class AuthStore {
    private static final String KEY_TOKEN = "access_token";
    private static final String KEY_EXPIRES = "expires";
    private static final String KEY_STATE = "state";

    private final SharedPreferences preferences;

    public AuthStore(Context context) {
        preferences = context
                .getSharedPreferences("auth", Context.MODE_PRIVATE);
    }

    public void setToken(String token) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_TOKEN, token);
        editor.apply();
    }

    public String getToken() {
        return preferences.getString(KEY_TOKEN, null);
    }

    public void setExpires(long milliseconds) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(KEY_EXPIRES, milliseconds);
        editor.apply();
    }

    public long getExpires() {
        return preferences.getLong(KEY_EXPIRES, -1);
    }

    public void setState(String state) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_STATE, state);
        editor.apply();
    }

    public String getState() {
        return preferences.getString(KEY_STATE, null);
    }
}
