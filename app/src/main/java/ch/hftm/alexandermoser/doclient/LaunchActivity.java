package ch.hftm.alexandermoser.doclient;

import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.util.UUID;

public class LaunchActivity extends AppCompatActivity {

    private AuthStore authStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_launch);

        authStore = new AuthStore(this);
        String token = authStore.getToken();
        long expires = authStore.getExpires();
        long now = System.currentTimeMillis();
        long nowAndOneDay = now + 24*60*60*1000; /* 1 day in ms */
        if (token == null || token.isEmpty() || nowAndOneDay > expires) {
            startOauth();
        } else {
            startActivity(new Intent(this, MainActivity.class));
        }
    }

    private void startOauth() {
        String state = UUID.randomUUID().toString();
        authStore.setState(state);
        Uri authorizationStart = new Uri.Builder()
                .scheme("https")
                .authority("cloud.digitalocean.com")
                .path("/v1/oauth/authorize")
                .appendQueryParameter("client_id", "0fe44336dea934a8bb822a8d0e7df7d0b92a209b9e2d87e81245ae5b4dbfde37")
                .appendQueryParameter("redirect_uri", "https://dgon.hftm.ch/")
                .appendQueryParameter("response_type", "token")
                .appendQueryParameter("scope", "read write")
                .appendQueryParameter("state", state)
                .build();
        startActivity(new Intent(Intent.ACTION_VIEW, authorizationStart));
    }
}
