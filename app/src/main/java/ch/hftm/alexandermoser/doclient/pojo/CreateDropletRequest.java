
package ch.hftm.alexandermoser.doclient.pojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "region",
    "size",
    "image",
    "ssh_keys",
    "backups",
    "ipv6",
    "user_data",
    "private_networking",
    "volumes",
    "tags"
})
public class CreateDropletRequest {

    @JsonProperty("name")
    private String name;
    @JsonProperty("region")
    private String region;
    @JsonProperty("size")
    private String size;
    @JsonProperty("image")
    private String image;
    @JsonProperty("ssh_keys")
    private List<Integer> sshKeys = null;
    @JsonProperty("backups")
    private Boolean backups;
    @JsonProperty("ipv6")
    private Boolean ipv6;
    @JsonProperty("user_data")
    private Object userData;
    @JsonProperty("private_networking")
    private boolean privateNetworking;
    @JsonProperty("monitoring")
    private boolean monitoring;
    @JsonProperty("volumes")
    private Object volumes;
    @JsonProperty("tags")
    private List<String> tags = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("region")
    public String getRegion() {
        return region;
    }

    @JsonProperty("region")
    public void setRegion(String region) {
        this.region = region;
    }

    @JsonProperty("size")
    public String getSize() {
        return size;
    }

    @JsonProperty("size")
    public void setSize(String size) {
        this.size = size;
    }

    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    @JsonProperty("ssh_keys")
    public List<Integer> getSshKeys() {
        return sshKeys;
    }

    @JsonProperty("ssh_keys")
    public void setSshKeys(List<Integer> sshKeys) {
        this.sshKeys = sshKeys;
    }

    @JsonProperty("backups")
    public Boolean getBackups() {
        return backups;
    }

    @JsonProperty("backups")
    public void setBackups(Boolean backups) {
        this.backups = backups;
    }

    @JsonProperty("ipv6")
    public Boolean getIpv6() {
        return ipv6;
    }

    @JsonProperty("ipv6")
    public void setIpv6(Boolean ipv6) {
        this.ipv6 = ipv6;
    }

    @JsonProperty("user_data")
    public Object getUserData() {
        return userData;
    }

    @JsonProperty("user_data")
    public void setUserData(Object userData) {
        this.userData = userData;
    }

    @JsonProperty("private_networking")
    public Object getPrivateNetworking() {
        return privateNetworking;
    }

    @JsonProperty("private_networking")
    public void setPrivateNetworking(boolean privateNetworking) {
        this.privateNetworking = privateNetworking;
    }

    @JsonProperty("monitoring")
    public Object getMonitoring() {
        return monitoring;
    }

    @JsonProperty("monitoring")
    public void setMonitoring(boolean monitoring) {
        this.monitoring = monitoring;
    }

    @JsonProperty("volumes")
    public Object getVolumes() {
        return volumes;
    }

    @JsonProperty("volumes")
    public void setVolumes(Object volumes) {
        this.volumes = volumes;
    }

    @JsonProperty("tags")
    public List<String> getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
