package ch.hftm.alexandermoser.doclient;

import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

public class OauthCallbackActivity extends AppCompatActivity {


    private static final String KEY_STATE = "state";
    private static final String KEY_ACCESS_TOKEN = "access_token";
    private static final String KEY_EXPIRES_IN = "expires_in";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oauth_callback);
        AuthStore authStore = new AuthStore(this);

        Uri data = getIntent().getData();
        if (data == null) {
            // TODO report to user: intent data is not a Uri;
            return;
        }

        Map<String, String> values = parseUrlFragment(data);

        if (!values.containsKey(KEY_STATE)) {
            // TODO report to user: "Callback fragment does not contain " + KEY_STATE;
            return;
        }

        String expectedState = authStore.getState();
        String actualState = values.get(KEY_STATE);

        if (!expectedState.equals(actualState)) {
            // TODO report to user: Callback states does not mach
            return;
        }

        if (!values.containsKey("access_token")) {
            // TODO report to user: "Callback fragment does not contain access_token";
            return;
        }

        if (!values.containsKey("expires_in")) {
            // TODO report to user: "Callback fragment does not contain expires_in";
            return;
        }

        long expiresInSeconds;
        try {
            expiresInSeconds = Integer.parseInt(Objects.requireNonNull(values.get(KEY_EXPIRES_IN)));

        } catch (NumberFormatException e) {
            // TODO report to user: "Callback fragment expires_in no valid integer
            return;
        }

        authStore.setExpires(System.currentTimeMillis() + (expiresInSeconds * 1000));
        authStore.setToken(values.get(KEY_ACCESS_TOKEN));
        startActivity(new Intent(this, MainActivity.class));
    }

    // https://stackoverflow.com/a/45450836
    public static Map<String, String> parseUrlFragment(Uri uri) {
        Map<String, String> output = new LinkedHashMap<>();
        String fragment = uri.getFragment();
        if (fragment == null) {
            return output;
        }

        String[] keys = fragment.split ("&");
        for (String key : keys) {
            String[] values = key.split ("=");
            output.put (values[0], (values.length > 1 ? values[1] : ""));
        }

        return output;
    }
}
