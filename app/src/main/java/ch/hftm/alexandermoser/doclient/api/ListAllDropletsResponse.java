
package ch.hftm.alexandermoser.doclient.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import ch.hftm.alexandermoser.doclient.pojo.Droplet;
import ch.hftm.alexandermoser.doclient.pojo.Links;
import ch.hftm.alexandermoser.doclient.pojo.Meta;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "droplets",
    "links",
    "meta"
})
public class ListAllDropletsResponse {

    @JsonProperty("droplets")
    private List<Droplet> droplets = null;
    @JsonProperty("links")
    private Links links;
    @JsonProperty("meta")
    private Meta meta;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("droplets")
    public List<Droplet> getDroplets() {
        return droplets;
    }

    @JsonProperty("droplets")
    public void setDroplets(List<Droplet> droplets) {
        this.droplets = droplets;
    }

    @JsonProperty("links")
    public Links getLinks() {
        return links;
    }

    @JsonProperty("links")
    public void setLinks(Links links) {
        this.links = links;
    }

    @JsonProperty("meta")
    public Meta getMeta() {
        return meta;
    }

    @JsonProperty("meta")
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
