package ch.hftm.alexandermoser.doclient.api;

import ch.hftm.alexandermoser.doclient.pojo.Action;
import ch.hftm.alexandermoser.doclient.pojo.ActionResponse;
import ch.hftm.alexandermoser.doclient.pojo.CreateDropletRequest;
import ch.hftm.alexandermoser.doclient.pojo.ImagesResponse;
import ch.hftm.alexandermoser.doclient.pojo.RegionsResponse;
import ch.hftm.alexandermoser.doclient.pojo.DropletResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface DigitalOceanApi {

    @GET("/v2/droplets")
    Call<ListAllDropletsResponse> listAllDroplets();

    @GET("/v2/droplets/{id}")
    Call<DropletResponse> retrieveExistingDropletById(@Path("id") int id);

    @POST("/v2/droplets/{id}/actions")
    Call<ActionResponse> action(@Path("id") int id, @Body Action action);

    @GET("/v2/regions")
    Call<RegionsResponse> listRegions();

    @GET("/v2/images?type=distribution")
    Call<ImagesResponse> listImages();

    @POST("/v2/droplets")
    Call<DropletResponse> createDroplet(@Body CreateDropletRequest request);
}
