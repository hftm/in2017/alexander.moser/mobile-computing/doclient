package ch.hftm.alexandermoser.doclient;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import ch.hftm.alexandermoser.doclient.api.DigitalOceanApi;
import ch.hftm.alexandermoser.doclient.api.DigitalOceanClientFactory;
import ch.hftm.alexandermoser.doclient.pojo.CreateDropletRequest;
import ch.hftm.alexandermoser.doclient.pojo.Droplet;
import ch.hftm.alexandermoser.doclient.pojo.DropletResponse;
import ch.hftm.alexandermoser.doclient.pojo.Image;
import ch.hftm.alexandermoser.doclient.pojo.ImagesResponse;
import ch.hftm.alexandermoser.doclient.pojo.Region;
import ch.hftm.alexandermoser.doclient.pojo.RegionsResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateDropletActivity extends AppCompatActivity {

    List<String> regions = new ArrayList<>();
    List<String> imageSlugs = new ArrayList<>();
    Dictionary<String, List<String>> sizesPerRegion = new Hashtable<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_droplet);

        final Spinner spinnerRegion = findViewById(R.id.region);
        final Spinner spinnerSize = findViewById(R.id.size);
        final Spinner spinnerImage = findViewById(R.id.image);

        spinnerRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String region = regions.get(position);
                List<String> sizes = sizesPerRegion.get(region);

                ArrayAdapter<String> adapter = new ArrayAdapter<>(CreateDropletActivity.this, R.layout.support_simple_spinner_dropdown_item, sizes);
                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                spinnerSize.setAdapter(adapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        final DigitalOceanApi apiClient = DigitalOceanClientFactory.build(this);
        apiClient.listRegions().enqueue(new Callback<RegionsResponse>() {
            @Override
            public void onResponse(Call<RegionsResponse> call, Response<RegionsResponse> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.list_regions_failed), Toast.LENGTH_LONG).show();
                    return;
                }

                regions.clear();
                for (Region region : response.body().getRegions()) {
                    String slug = region.getSlug();
                    regions.add(slug);
                    sizesPerRegion.put(slug, region.getSizes());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(CreateDropletActivity.this, R.layout.support_simple_spinner_dropdown_item, regions);
                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                spinnerRegion.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<RegionsResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        apiClient.listImages().enqueue(new Callback<ImagesResponse>() {
            @Override
            public void onResponse(Call<ImagesResponse> call, Response<ImagesResponse> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.list_images_failed), Toast.LENGTH_LONG).show();
                    return;
                }

                imageSlugs.clear();
                for (Image image : response.body().getImages()) {
                    String slug = image.getSlug();
                    imageSlugs.add(slug);
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(CreateDropletActivity.this, R.layout.support_simple_spinner_dropdown_item, imageSlugs);
                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                spinnerImage.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<ImagesResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        findViewById(R.id.create).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateDropletRequest request = new CreateDropletRequest();

                EditText name = findViewById(R.id.name);
                request.setName(name.getText().toString());
                request.setRegion(regions.get(spinnerRegion.getSelectedItemPosition()));
                request.setSize((String) spinnerSize.getSelectedItem());
                request.setImage(imageSlugs.get(spinnerImage.getSelectedItemPosition()));

                CheckBox backups = findViewById(R.id.backups);
                request.setBackups(backups.isChecked());

                CheckBox ipv6 = findViewById(R.id.ipv6);
                request.setIpv6(ipv6.isChecked());

                CheckBox privateNetworking = findViewById(R.id.private_networking);
                request.setPrivateNetworking(privateNetworking.isChecked());

                CheckBox monitoring = findViewById(R.id.monitoring);
                request.setMonitoring(monitoring.isChecked());

                EditText tagsInput = findViewById(R.id.tags);
                List<String> tags = new ArrayList<>();
                for (String tag : tagsInput.getText().toString().split(",")) {
                    tags.add(tag.trim());
                }
                request.setTags(tags);

                apiClient.createDroplet(request).enqueue(new Callback<DropletResponse>() {
                    @Override
                    public void onResponse(Call<DropletResponse> call, Response<DropletResponse> response) {
                        if (response.isSuccessful()) {
                            Droplet droplet = response.body().getDroplet();
                            Intent intent = new Intent(CreateDropletActivity.this, DropletViewActivity.class);
                            intent.putExtra(DropletViewActivity.INTENT_DROPLET_ID, droplet.getId());
                            CreateDropletActivity.this.startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.droplet_creation_failed), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DropletResponse> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}
