
package ch.hftm.alexandermoser.doclient.pojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "v4",
    "v6"
})
public class Networks {

    @JsonProperty("v4")
    private List<V4> v4 = null;
    @JsonProperty("v6")
    private List<V6> v6 = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("v4")
    public List<V4> getV4() {
        return v4;
    }

    @JsonProperty("v4")
    public void setV4(List<V4> v4) {
        this.v4 = v4;
    }

    @JsonProperty("v6")
    public List<V6> getV6() {
        return v6;
    }

    @JsonProperty("v6")
    public void setV6(List<V6> v6) {
        this.v6 = v6;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
