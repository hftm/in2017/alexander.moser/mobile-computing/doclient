package ch.hftm.alexandermoser.doclient.droplets;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ch.hftm.alexandermoser.doclient.DropletViewActivity;
import ch.hftm.alexandermoser.doclient.R;
import ch.hftm.alexandermoser.doclient.pojo.Droplet;

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.ViewHolder> {
    private static final String TAG = "RecycleViewAdapter";

    private final List<Droplet> droplets;
    private final Context context;

    public RecycleViewAdapter(List<Droplet> droplets, Context context) {
        this.droplets = droplets;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_dropletlistitem, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");
        final Droplet droplet = droplets.get(position);
        holder.imageView.setImageResource(R.drawable.ic_droplet);
        holder.nameTextView.setText(droplet.getName());
        holder.descriptionTextView.setText(droplet.getRegion().getSlug().toUpperCase()  + " / " +
                String.format("%s", droplet.getMemory() / 1024) + "GB / "+
                String.format("%s", droplet.getDisk()) + "GB Disk");
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), DropletViewActivity.class);
                intent.putExtra(DropletViewActivity.INTENT_DROPLET_ID, droplet.getId());
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return droplets.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView nameTextView;
        TextView descriptionTextView;
        RelativeLayout layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image);
            nameTextView = itemView.findViewById(R.id.droplet_name);
            descriptionTextView = itemView.findViewById(R.id.droplet_description);
            layout = itemView.findViewById(R.id.parentLayout);
        }
    }
}
