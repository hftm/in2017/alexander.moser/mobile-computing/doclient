
package ch.hftm.alexandermoser.doclient.pojo;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "slug",
    "memory",
    "vcpus",
    "disk",
    "transfer",
    "price_monthly",
    "price_hourly",
    "regions",
    "available"
})
public class Size {

    @JsonProperty("slug")
    private String slug;
    @JsonProperty("memory")
    private Integer memory;
    @JsonProperty("vcpus")
    private Integer vcpus;
    @JsonProperty("disk")
    private Integer disk;
    @JsonProperty("transfer")
    private Float transfer;
    @JsonProperty("price_monthly")
    private Float priceMonthly;
    @JsonProperty("price_hourly")
    private Float priceHourly;
    @JsonProperty("regions")
    private List<String> regions = null;
    @JsonProperty("available")
    private Boolean available;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("slug")
    public String getSlug() {
        return slug;
    }

    @JsonProperty("slug")
    public void setSlug(String slug) {
        this.slug = slug;
    }

    @JsonProperty("memory")
    public Integer getMemory() {
        return memory;
    }

    @JsonProperty("memory")
    public void setMemory(Integer memory) {
        this.memory = memory;
    }

    @JsonProperty("vcpus")
    public Integer getVcpus() {
        return vcpus;
    }

    @JsonProperty("vcpus")
    public void setVcpus(Integer vcpus) {
        this.vcpus = vcpus;
    }

    @JsonProperty("disk")
    public Integer getDisk() {
        return disk;
    }

    @JsonProperty("disk")
    public void setDisk(Integer disk) {
        this.disk = disk;
    }

    @JsonProperty("transfer")
    public Float getTransfer() {
        return transfer;
    }

    @JsonProperty("transfer")
    public void setTransfer(Float transfer) {
        this.transfer = transfer;
    }

    @JsonProperty("price_monthly")
    public Float getPriceMonthly() {
        return priceMonthly;
    }

    @JsonProperty("price_monthly")
    public void setPriceMonthly(Float priceMonthly) {
        this.priceMonthly = priceMonthly;
    }

    @JsonProperty("price_hourly")
    public Float getPriceHourly() {
        return priceHourly;
    }

    @JsonProperty("price_hourly")
    public void setPriceHourly(Float priceHourly) {
        this.priceHourly = priceHourly;
    }

    @JsonProperty("regions")
    public List<String> getRegions() {
        return regions;
    }

    @JsonProperty("regions")
    public void setRegions(List<String> regions) {
        this.regions = regions;
    }

    @JsonProperty("available")
    public Boolean getAvailable() {
        return available;
    }

    @JsonProperty("available")
    public void setAvailable(Boolean available) {
        this.available = available;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @NonNull
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%s", this.vcpus));
        if (this.vcpus == 1) {
            sb.append(" vCPU");
        } else {
            sb.append(" vCPUs");
        }
        sb.append(" / ");
        sb.append(String.format("%s", this.memory / 1024));
        sb.append(" GB");
        sb.append(" / ");
        sb.append(String.format("%s", this.disk));
        sb.append(" GB Disk");
        sb.append(" / ");
        sb.append(String.format("%s", (int) Math.round(this.transfer)));
        sb.append(" TB Transfer");
        return sb.toString();
    }
}
