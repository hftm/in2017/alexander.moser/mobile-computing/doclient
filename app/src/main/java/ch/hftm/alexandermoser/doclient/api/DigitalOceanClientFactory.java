package ch.hftm.alexandermoser.doclient.api;

import android.content.Context;

import java.io.IOException;

import ch.hftm.alexandermoser.doclient.AuthStore;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class DigitalOceanClientFactory {
    public static DigitalOceanApi build(Context context) {

        final String accessToken = new AuthStore(context).getToken();

        OkHttpClient defaultHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Interceptor.Chain chain) throws IOException {
                        Request authorisedRequest = chain.request().newBuilder().addHeader("Authorization", "Bearer " + accessToken).build();
                        return chain.proceed(authorisedRequest);
                    }}).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.digitalocean.com/") //BaseURL always ends with "/"
                .addConverterFactory(JacksonConverterFactory.create())
                .client(defaultHttpClient)
                .build();

        return retrofit.create(DigitalOceanApi.class);
    }
}
