
package ch.hftm.alexandermoser.doclient.pojo;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "status",
    "type",
    "started_at",
    "completed_at",
    "resource_id",
    "resource_type"
})
public class Action {

    public static final String REBOOT = "reboot";
    public static final String POWER_CYCLE = "power_cycle";
    public static final String SHUTDOWN = "shutdown";
    public static final String POWER_OFF = "power_off";
    public static final String POWER_ON = "power_on";
    public static final String RESIZE = "resize";
    public static final String RENAME = "rename";

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("status")
    private String status;
    @JsonProperty("type")
    private String type;
    @JsonProperty("started_at")
    private String startedAt;
    @JsonProperty("completed_at")
    private Object completedAt;
    @JsonProperty("resource_id")
    private Integer resourceId;
    @JsonProperty("resource_type")
    private String resourceType;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("started_at")
    public String getStartedAt() {
        return startedAt;
    }

    @JsonProperty("started_at")
    public void setStartedAt(String startedAt) {
        this.startedAt = startedAt;
    }

    @JsonProperty("completed_at")
    public Object getCompletedAt() {
        return completedAt;
    }

    @JsonProperty("completed_at")
    public void setCompletedAt(Object completedAt) {
        this.completedAt = completedAt;
    }

    @JsonProperty("resource_id")
    public Integer getResourceId() {
        return resourceId;
    }

    @JsonProperty("resource_id")
    public void setResourceId(Integer resourceId) {
        this.resourceId = resourceId;
    }

    @JsonProperty("resource_type")
    public String getResourceType() {
        return resourceType;
    }

    @JsonProperty("resource_type")
    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
