package ch.hftm.alexandermoser.doclient;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringJoiner;

import ch.hftm.alexandermoser.doclient.api.DigitalOceanApi;
import ch.hftm.alexandermoser.doclient.api.DigitalOceanClientFactory;
import ch.hftm.alexandermoser.doclient.pojo.Action;
import ch.hftm.alexandermoser.doclient.pojo.ActionResponse;
import ch.hftm.alexandermoser.doclient.pojo.Droplet;
import ch.hftm.alexandermoser.doclient.pojo.DropletResponse;
import ch.hftm.alexandermoser.doclient.pojo.V4;
import ch.hftm.alexandermoser.doclient.pojo.V6;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DropletViewActivity extends AppCompatActivity {

    public static final String INTENT_DROPLET_ID = "ch.hftm.alexandermoser.doclient.DropletViewActivity.DROPLET_ID";

    private DigitalOceanApi apiClient;
    private int dropletId;
    private Spinner spinnerSize;
    private EditText editTextRename;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_droplet_view);

        spinnerSize = findViewById(R.id.spinner_size);
        editTextRename = findViewById(R.id.editText_rename);
        dropletId = getIntent().getIntExtra(INTENT_DROPLET_ID, 0);
        apiClient = DigitalOceanClientFactory.build(this);
        apiClient.retrieveExistingDropletById(dropletId).enqueue(new Callback<DropletResponse>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<DropletResponse> call, Response<DropletResponse> response) {
                Droplet droplet = response.body().getDroplet();

                ArrayAdapter<String> adapter = new ArrayAdapter<>(DropletViewActivity.this, R.layout.support_simple_spinner_dropdown_item, droplet.getRegion().getSizes());
                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                spinnerSize.setAdapter(adapter);

                TextView textViewName = findViewById(R.id.textView_name);
                String name = droplet.getName();
                textViewName.setText(name);
                editTextRename.setText(name);

                TextView textViewSize = findViewById(R.id.textView_size);
                textViewSize.setText(droplet.getSize().toString());

                TextView textViewRegion = findViewById(R.id.textView_region);
                textViewRegion.setText(droplet.getRegion().getName());

                TextView textViewImage = findViewById(R.id.textView_image);
                textViewImage.setText(droplet.getImage().getDistribution() + " " + droplet.getImage().getName());

                StringJoiner ipv4StringJoiner = new StringJoiner(System.lineSeparator());
                for (V4 network : droplet.getNetworks().getV4()) {
                    ipv4StringJoiner.add(network.getIpAddress());
                }
                TextView textViewIpv4 = findViewById(R.id.textView_ipv4);
                textViewIpv4.setText(ipv4StringJoiner.toString());

                StringJoiner ipv6StringJoiner = new StringJoiner(System.lineSeparator());
                for (V6 network : droplet.getNetworks().getV6()) {
                    ipv6StringJoiner.add(network.getIpAddress());
                }
                TextView textViewIpv6 = findViewById(R.id.textView_ipv6);
                textViewIpv6.setText(ipv6StringJoiner.toString());

                TextView textViewCreatedAt = findViewById(R.id.textView_createdAt);
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                String createdAtString = droplet.getCreatedAt();
                try {
                    Date createdAt = df.parse(createdAtString);
                    DateFormat dateFormat = android.text.format.DateFormat.getLongDateFormat(getApplicationContext());
                    DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(getApplicationContext());
                    textViewCreatedAt.setText(dateFormat.format(createdAt) + " " + timeFormat.format(createdAt));
                } catch (ParseException e) {
                    textViewCreatedAt.setText(createdAtString);
                }

                TextView textViewStatus = findViewById(R.id.textView_status);
                textViewStatus.setText(droplet.getStatus());

                TextView textViewTags = findViewById(R.id.textView_tags);
                textViewTags.setText(TextUtils.join(", ", droplet.getTags()));
            }

            @Override
            public void onFailure(Call<DropletResponse> call, Throwable t) {
                Toast.makeText(DropletViewActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        findViewById(R.id.button_reboot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dropletAction(Action.REBOOT);
            }
        });
        findViewById(R.id.button_power_cycle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dropletAction(Action.POWER_CYCLE);
            }
        });
        findViewById(R.id.button_shutdown).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dropletAction(Action.SHUTDOWN);
            }
        });
        findViewById(R.id.button_power_off).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dropletAction(Action.POWER_OFF);
            }
        });
        findViewById(R.id.button_power_on).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dropletAction(Action.POWER_ON);
            }
        });
        findViewById(R.id.button_resize).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Action action = new Action();
                action.setType(Action.RESIZE);
                String size = (String) spinnerSize.getSelectedItem();
                action.setAdditionalProperty("size", size);
                dropletAction(action);
            }
        });
        findViewById(R.id.button_rename).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Action action = new Action();
                action.setType(Action.RENAME);
                String name = editTextRename.getText().toString();
                action.setAdditionalProperty("name", name);
                dropletAction(action);
            }
        });
    }

    private void dropletAction(String type) {
        Action action = new Action();
        action.setType(type);
        dropletAction(action);
    }

    private void dropletAction(Action action) {
        apiClient.action(dropletId, action).enqueue(new Callback<ActionResponse>() {
            @Override
            public void onResponse(Call<ActionResponse> call, Response<ActionResponse> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.action_successful), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.action_fail), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ActionResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
