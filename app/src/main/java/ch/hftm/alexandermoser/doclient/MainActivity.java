package ch.hftm.alexandermoser.doclient;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import java.util.ArrayList;
import java.util.List;

import ch.hftm.alexandermoser.doclient.api.DigitalOceanApi;
import ch.hftm.alexandermoser.doclient.api.DigitalOceanClientFactory;
import ch.hftm.alexandermoser.doclient.api.ListAllDropletsResponse;
import ch.hftm.alexandermoser.doclient.droplets.RecycleViewAdapter;
import ch.hftm.alexandermoser.doclient.pojo.Droplet;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    List<Droplet> droplets = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final View parentLayout = findViewById(R.id.parentLayout);
        final RecycleViewAdapter adapter = new RecycleViewAdapter(droplets, this);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        findViewById(R.id.fab_create_droplet).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), CreateDropletActivity.class);
                v.getContext().startActivity(intent);
            }
        });

        DigitalOceanApi apiClient = DigitalOceanClientFactory.build(MainActivity.this);
        apiClient.listAllDroplets().enqueue(new Callback<ListAllDropletsResponse>() {
            @Override
            public void onResponse(Call<ListAllDropletsResponse> call, Response<ListAllDropletsResponse> response) {
                if (response.isSuccessful()) {
                    droplets.addAll(response.body().getDroplets());
                    adapter.notifyDataSetChanged();
                } else {
                    Snackbar.make(parentLayout, response.toString(), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }

            @Override
            public void onFailure(Call<ListAllDropletsResponse> call, Throwable t) {
                Snackbar.make(parentLayout, t.getMessage(), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }
}
