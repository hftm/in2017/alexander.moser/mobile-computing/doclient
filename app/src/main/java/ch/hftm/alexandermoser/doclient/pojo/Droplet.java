
package ch.hftm.alexandermoser.doclient.pojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "memory",
    "vcpus",
    "disk",
    "locked",
    "status",
    "kernel",
    "created_at",
    "features",
    "backup_ids",
    "next_backup_window",
    "snapshot_ids",
    "image",
    "volume_ids",
    "size",
    "size_slug",
    "networks",
    "region",
    "tags"
})
public class Droplet {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("memory")
    private Integer memory;
    @JsonProperty("vcpus")
    private Integer vcpus;
    @JsonProperty("disk")
    private Integer disk;
    @JsonProperty("locked")
    private Boolean locked;
    @JsonProperty("status")
    private String status;
    @JsonProperty("kernel")
    private Object kernel;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("features")
    private List<String> features = null;
    @JsonProperty("backup_ids")
    private List<Object> backupIds = null;
    @JsonProperty("next_backup_window")
    private Object nextBackupWindow;
    @JsonProperty("snapshot_ids")
    private List<Integer> snapshotIds = null;
    @JsonProperty("image")
    private Image image;
    @JsonProperty("volume_ids")
    private List<Object> volumeIds = null;
    @JsonProperty("size")
    private Size size;
    @JsonProperty("size_slug")
    private String sizeSlug;
    @JsonProperty("networks")
    private Networks networks;
    @JsonProperty("region")
    private Region region;
    @JsonProperty("tags")
    private List<String> tags = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("memory")
    public Integer getMemory() {
        return memory;
    }

    @JsonProperty("memory")
    public void setMemory(Integer memory) {
        this.memory = memory;
    }

    @JsonProperty("vcpus")
    public Integer getVcpus() {
        return vcpus;
    }

    @JsonProperty("vcpus")
    public void setVcpus(Integer vcpus) {
        this.vcpus = vcpus;
    }

    @JsonProperty("disk")
    public Integer getDisk() {
        return disk;
    }

    @JsonProperty("disk")
    public void setDisk(Integer disk) {
        this.disk = disk;
    }

    @JsonProperty("locked")
    public Boolean getLocked() {
        return locked;
    }

    @JsonProperty("locked")
    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("kernel")
    public Object getKernel() {
        return kernel;
    }

    @JsonProperty("kernel")
    public void setKernel(Object kernel) {
        this.kernel = kernel;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("features")
    public List<String> getFeatures() {
        return features;
    }

    @JsonProperty("features")
    public void setFeatures(List<String> features) {
        this.features = features;
    }

    @JsonProperty("backup_ids")
    public List<Object> getBackupIds() {
        return backupIds;
    }

    @JsonProperty("backup_ids")
    public void setBackupIds(List<Object> backupIds) {
        this.backupIds = backupIds;
    }

    @JsonProperty("next_backup_window")
    public Object getNextBackupWindow() {
        return nextBackupWindow;
    }

    @JsonProperty("next_backup_window")
    public void setNextBackupWindow(Object nextBackupWindow) {
        this.nextBackupWindow = nextBackupWindow;
    }

    @JsonProperty("snapshot_ids")
    public List<Integer> getSnapshotIds() {
        return snapshotIds;
    }

    @JsonProperty("snapshot_ids")
    public void setSnapshotIds(List<Integer> snapshotIds) {
        this.snapshotIds = snapshotIds;
    }

    @JsonProperty("image")
    public Image getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(Image image) {
        this.image = image;
    }

    @JsonProperty("volume_ids")
    public List<Object> getVolumeIds() {
        return volumeIds;
    }

    @JsonProperty("volume_ids")
    public void setVolumeIds(List<Object> volumeIds) {
        this.volumeIds = volumeIds;
    }

    @JsonProperty("size")
    public Size getSize() {
        return size;
    }

    @JsonProperty("size")
    public void setSize(Size size) {
        this.size = size;
    }

    @JsonProperty("size_slug")
    public String getSizeSlug() {
        return sizeSlug;
    }

    @JsonProperty("size_slug")
    public void setSizeSlug(String sizeSlug) {
        this.sizeSlug = sizeSlug;
    }

    @JsonProperty("networks")
    public Networks getNetworks() {
        return networks;
    }

    @JsonProperty("networks")
    public void setNetworks(Networks networks) {
        this.networks = networks;
    }

    @JsonProperty("region")
    public Region getRegion() {
        return region;
    }

    @JsonProperty("region")
    public void setRegion(Region region) {
        this.region = region;
    }

    @JsonProperty("tags")
    public List<String> getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
