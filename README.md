# DigitalOcean Client

For evaluation purposes, you can use the credentials provided by the link below.  
**Please remember to delete all your created droplets once your done**, in order to prevent unnececcary costs.

https://hftm-my.sharepoint.com/:t:/g/personal/alexander_moser_hftm_ch/EWTyssvgEvVLqprgCjule-wBNLtfRMOil437fEXa2EYKcg?e=BbcJcH

When creating Droplets, please make sure to select an appropriate size as they can cost up to $1440 per month / $2.143 per hour.  
The recommended size is `1gb`.

The droplet name must be a valid DNS record. It is recommended that you only use lower case letters (`[a-z]`)), dashes (`-`) and underscores (`_`).

Due to a bug in the implementation, a droplet must be assigned at least one tag. Multiple tags can be assigned by seperating them by comma.
